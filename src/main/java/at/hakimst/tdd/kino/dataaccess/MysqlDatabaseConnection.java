package at.hakimst.tdd.kino.dataaccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlDatabaseConnection {
    private static Connection con = null;

    private MysqlDatabaseConnection() {
    }

    public static Connection getConnection(String url, String user, String pwd) throws ClassNotFoundException, SQLException {
        if (con == null) {
            // evtl. notwendig, sonst auskommentieren
            Class.forName ("org.h2.Driver");

            con = DriverManager.getConnection(url, user, pwd);
        }
        return con;
    }
}
